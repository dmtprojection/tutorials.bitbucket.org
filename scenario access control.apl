.head 0 +  Application Description: 
Copyright 2001 by Projection Ltd

Module:		Access Control
Type:		Library
Target:		APD
Description:	This APL is used to create an APD that allows system administrators 
		to control the value of the master access IDs and passwords used by Scenario applications
.data CLASSPROPSSIZE
0000: 9600
.enddata
.data CLASSPROPS
0000: 4400420047005F00 50004C0041005900 4200410043004B00 5F00470055004900
0020: 44000000FFFE1000 3C3F6A28FAF3E446 AFAA0E214780E244 5500730065004700
0040: 6C006F0062006100 6C00500061007400 68000000FFFE0400 0100000041007000
0060: 7000500061007400 68004C0065006E00 0000FFFE04000000 0000410070007000
0080: 5000610074006800 0000FFFE00000000 000000000000
.enddata
.head 1 -  Outline Version - 4.0.52
.head 1 +  Design-time Settings
.data VIEWINFO
0000: 6F00000001000000 FFFF01000D004347 5458566965775374 6174650400010000
0020: 0000000000A50000 002C000000020000 0003000000FFFFFF FFFFFFFFFFF8FFFF
0040: FFE1FFFFFFFFFFFF FF000000007C0200 004D010000010000 0001000000010000
0060: 00FFFEFF0F410070 0070006C00690063 006100740069006F 006E004900740065
0080: 006D0000000000
.enddata
.data DT_MAKERUNDLG
0000: 1300001002000000 FFFEFF00FFFEFF2F 43003A005C005000 52004F0047005200
0020: 41007E0031005C00 430045004E005400 5500520041005C00 4300540044003100
0040: 350031005C005400 65006D0070006C00 6100740065007300 5C006E0065007700
0060: 6100700070002E00 650078006500FFFE FF2F43003A005C00 500052004F004700
0080: 520041007E003100 5C00430045004E00 5400550052004100 5C00430054004400
00A0: 3100350031005C00 540065006D007000 6C00610074006500 73005C006E006500
00C0: 7700610070007000 2E0064006C006C00 FFFEFF2F43003A00 5C00500052004F00
00E0: 4700520041007E00 31005C0043004500 4E00540055005200 41005C0043005400
0100: 4400310035003100 5C00540065006D00 70006C0061007400 650073005C006E00
0120: 6500770061007000 70002E0061007000 6300000001010100 64000000FFFEFF2F
0140: 43003A005C005000 52004F0047005200 41007E0031005C00 430045004E005400
0160: 5500520041005C00 4300540044003100 350031005C005400 65006D0070006C00
0180: 6100740065007300 5C006E0065007700 6100700070002E00 720075006E00FFFE
01A0: FF2F43003A005C00 500052004F004700 520041007E003100 5C00430045004E00
01C0: 5400550052004100 5C00430054004400 3100350031005C00 540065006D007000
01E0: 6C00610074006500 73005C006E006500 7700610070007000 2E0064006C006C00
0200: FFFEFF2F43003A00 5C00500052004F00 4700520041007E00 31005C0043004500
0220: 4E00540055005200 41005C0043005400 4400310035003100 5C00540065006D00
0240: 70006C0061007400 650073005C006E00 6500770061007000 70002E0061007000
0260: 6300000001010100 64000000FFFEFF1B 5300630065006E00 6100720069006F00
0280: 2000410063006300 6500730073002000 43006F006E007400 72006F006C002E00
02A0: 610070006400FFFE FF0A6E0065007700 6100700070002E00 64006C006C00FFFE
02C0: FF0A6E0065007700 6100700070002E00 6100700063000001 0101010064000000
02E0: FFFEFF2F43003A00 5C00500052004F00 4700520041007E00 31005C0043004500
0300: 4E00540055005200 41005C0043005400 4400310035003100 5C00540065006D00
0320: 70006C0061007400 650073005C006E00 6500770061007000 70002E0061007000
0340: 6C00FFFEFF2F4300 3A005C0050005200 4F00470052004100 7E0031005C004300
0360: 45004E0054005500 520041005C004300 5400440031003500 31005C0054006500
0380: 6D0070006C006100 7400650073005C00 6E00650077006100 700070002E006400
03A0: 6C006C00FFFEFF2F 43003A005C005000 52004F0047005200 41007E0031005C00
03C0: 430045004E005400 5500520041005C00 4300540044003100 350031005C005400
03E0: 65006D0070006C00 6100740065007300 5C006E0065007700 6100700070002E00
0400: 6100700063000000 0101010064000000 FFFEFF00FFFEFF00 FFFEFF0000000101
0420: 010064000000FFFE FF00FFFEFF00FFFE FF00000001010100 64000000FFFEFF00
0440: FFFEFF00FFFEFF00 0000010101006400 00000000000001FF FEFF00FFFEFF00FF
0460: FEFF00FFFEFF00FF FEFF00FFFEFF0000 00000000000000FF FEFF00FFFEFF00FF
0480: FEFF000000000000 0100000001000000 01FFFEFF00010000 0000000000FFFEFF
04A0: 0001000010000000 0000000000000000 0000000000000000 0001000000010000
04C0: 00FFFEFF00FFFEFF 00FFFEFF00FFFEFF 00FFFEFF00FFFEFF 00FFFEFF00FFFEFF
04E0: 00FFFEFF00000000 0000000000000000 00FFFEFF00010000 0001000000FFFEFF
0500: 00
.enddata
.head 2 -  Outline Window State: Normal
.head 2 +  Outline Window Location and Size
.data VIEWINFO
0000: 6600040003002D00 0000000000000000 0000B71E5D0E0500 1D00FFFF4D61696E
0020: 0000000000000000 0000000000000000 0000003B00010000 00000000000000E9
0040: 1E800A00008600FF FF496E7465726E61 6C2046756E637469 6F6E730000000000
0060: 0000000000000000 0000000000003200 0100000000000000 0000E91E800A0000
0080: DF00FFFF56617269 61626C6573000000 0000000000000000 0000000000000000
00A0: 3000010000000000 00000000F51E100D 0000F400FFFF436C 6173736573000000
00C0: 0000000000000000 0000000000000000
.enddata
.data VIEWSIZE
0000: D000
.enddata
.head 3 -  Left: -0.013"
.head 3 -  Top: 0.0"
.head 3 -  Width:  8.013"
.head 3 -  Height: 4.969"
.head 2 +  Options Box Location
.data VIEWINFO
0000: D4180909B80B1A00
.enddata
.data VIEWSIZE
0000: 0800
.enddata
.head 3 -  Visible? Yes
.head 3 -  Left: 4.15"
.head 3 -  Top: 1.885"
.head 3 -  Width:  3.8"
.head 3 -  Height: 2.073"
.head 2 +  Class Editor Location
.head 3 -  Visible? No
.head 3 -  Left: 0.575"
.head 3 -  Top: 0.094"
.head 3 -  Width:  5.063"
.head 3 -  Height: 2.719"
.head 2 +  Tool Palette Location
.head 3 -  Visible? No
.head 3 -  Left: 6.388"
.head 3 -  Top: 0.729"
.head 2 -  Fully Qualified External References? Yes
.head 2 -  Reject Multiple Window Instances? No
.head 2 -  Enable Runtime Checks Of External References? Yes
.head 2 -  Use Release 4.0 Scope Rules? No
.head 2 -  Edit Fields Read Only On Disable? No
.head 2 -  Assembly Symbol File:
.head 1 -  Libraries
.head 1 +  Global Declarations
.head 2 +  Window Defaults
.head 3 +  Tool Bar
.head 4 -  Display Style? Standard
.head 4 -  Display Style? Etched
.head 4 -  Font Name: MS Sans Serif
.head 4 -  Font Size: 8
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Form Window
.head 4 -  Display Style? Etched
.head 4 -  Font Name: MS Sans Serif
.head 4 -  Font Size: 8
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Dialog Box
.head 4 -  Display Style? Etched
.head 4 -  Font Name: MS Sans Serif
.head 4 -  Font Size: 8
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Top Level Table Window
.head 4 -  Font Name: MS Sans Serif
.head 4 -  Font Size: 8
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Top Level Grid Window
.head 4 -  Font Name: System Default
.head 4 -  Font Size: System Default
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Data Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Multiline Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Spin Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Background Text
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Pushbutton
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Radio Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Check Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Option Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Group Box
.head 4 -  GroupBox Style: Etched
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Line Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Child Table Window
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  List Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Combo Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Line
.head 4 -  Line Color: Use Parent
.head 3 +  Frame
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: 3D Face Color
.head 3 +  Picture
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Date Time Picker
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Child Grid Window
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Rich Text Control
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Date Picker
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 3 +  Tree Control
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Flow Direction: Default
.head 3 +  Navigation Bar
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 4 -  Flow Direction: Default
.head 3 -  Pane Separator
.head 3 +  Tab Bar
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Progress Bar
.head 4 -  Background Color: Use Parent
.head 2 +  Formats
.head 3 -  Number: 0'%'
.head 3 -  Number: #0
.head 3 -  Number: ###000
.head 3 -  Number: ###000;'($'###000')'
.head 3 -  Date/Time: hh:mm:ss AMPM
.head 3 -  Date/Time: M/d/yy
.head 3 -  Date/Time: MM-dd-yy
.head 3 -  Date/Time: dd-MMM-yyyy
.head 3 -  Date/Time: MMM d, yyyy
.head 3 -  Date/Time: MMM d, yyyy hh:mm AMPM
.head 3 -  Date/Time: MMMM d, yyyy hh:mm AMPM
.head 2 -  External Functions
.head 2 -  External Assemblies
.head 2 +  Constants
.data CCDATA
0000: 3000000000000000 0000000000000000 0000000000000000 0000000000000000
0020: 0000000000000000
.enddata
.data CCSIZE
0000: 2800
.enddata
.head 3 -  System
.head 3 +  User
.head 4 -  String: PRJ_MasterID	= "sysadm"
.head 4 -  String: PRJ_Password	= "sysadm"
.head 4 -  String: SIR_MasterID	= "SIR"
.head 4 -  String: SIR_Password	= "SIR"
.head 4 -  String: TIM_MasterID	= "timdb"
.head 4 -  String: TIM_Password	= "timdb"
.head 4 -  String: EDS_MasterID	= "EDS"
.head 4 -  String: EDS_Password	= "EDS"
.head 4 -  ! DSR9241 Added for secure connection sending an email - needed the user id and password for SMTP connection
.head 4 -  String: SMTP_MasterID	= "SMTP"
.head 4 -  String: SMTP_Password	= "SMTP"
.head 4 -  ! DSR12894 Added AP SMTP and AR SMTP
.head 4 -  String: SMTP_AP_MasterID	= "SMTP_AP"
.head 4 -  String: SMTP_AP_Password	= "SMTP_AP"
.head 4 -  String: SMTP_AR_MasterID	= "SMTP_AR"
.head 4 -  String: SMTP_AR_Password	= "SMTP_AR"
.head 3 -  Enumerations
.head 2 -  Resources
.head 2 -  Variables
.head 2 +  Internal Functions
.head 3 +  Function: fnMasterAccess			! __Exported
.head 4 -  Description:
.head 4 -  Returns
.head 4 +  Parameters
.head 5 -  String: psSystem
.head 5 -  Receive String: psUserID
.head 5 -  Receive String: psPassword
.head 4 -  Static Variables
.head 4 -  Local variables
.head 4 +  Actions
.head 5 +  If psSystem = "PRJ"
.head 6 -  Set psUserID = PRJ_MasterID
.head 6 -  Set psPassword = PRJ_Password
.head 5 +  Else If psSystem = "SIR"
.head 6 -  Set psUserID = SIR_MasterID
.head 6 -  Set psPassword = SIR_Password
.head 5 +  Else If psSystem = "TIM"
.head 6 -  Set psUserID = TIM_MasterID
.head 6 -  Set psPassword = TIM_Password
.head 5 -  ! DSR9241 Added for secure connection sending an email - needed the user id and password for SMTP connection
.head 5 +  Else If psSystem = "SMTP"
.head 6 -  Set psUserID = SMTP_MasterID
.head 6 -  Set psPassword = SMTP_Password
.head 5 -  ! DSR12894 Added AP SMTP and AR SMTP
.head 5 +  Else If psSystem = "SMTP_AP"
.head 6 -  Set psUserID = SMTP_AP_MasterID
.head 6 -  Set psPassword = SMTP_AP_Password
.head 5 +  Else If psSystem = "SMTP_AR"
.head 6 -  Set psUserID = SMTP_AR_MasterID
.head 6 -  Set psPassword = SMTP_AR_Password
.head 5 -  !
.head 5 -  ! SRI-ACROW-0395 RTF - Added
.head 5 +  Else If psSystem = "EDS"
.head 6 -  Set psUserID = EDS_MasterID
.head 6 -  Set psPassword = EDS_Password
.head 5 -  !
.head 2 -  Named Exceptions
.head 2 -  Named Toolbars
.head 2 -  Named Menus
.head 2 -  Class Definitions
.head 2 +  Default Classes
.head 3 -  MDI Window: cBaseMDI
.head 3 -  Form Window:
.head 3 -  Dialog Box:
.head 3 -  Table Window:
.head 3 -  Grid Window:
.head 3 -  Quest Window:
.head 3 -  Data Field:
.head 3 -  Spin Field:
.head 3 -  Multiline Field:
.head 3 -  Pushbutton:
.head 3 -  Radio Button:
.head 3 -  Option Button:
.head 3 -  Date Picker:
.head 3 -  Date Time Picker:
.head 3 -  Child Grid:
.head 3 -  Tab Bar:
.head 3 -  Rich Text Control:
.head 3 -  Separator:
.head 3 -  Tree Control:
.head 3 -  Navigation Bar:
.head 3 -  Pane Separator:
.head 3 -  Progress Bar:
.head 3 -  Check Box:
.head 3 -  Child Table:
.head 3 -  Quest Child Window: cQuickDatabase
.head 3 -  List Box:
.head 3 -  Combo Box:
.head 3 -  Picture:
.head 3 -  Vertical Scroll Bar:
.head 3 -  Horizontal Scroll Bar:
.head 3 -  Column:
.head 3 -  Background Text:
.head 3 -  Group Box:
.head 3 -  Line:
.head 3 -  Frame:
.head 3 -  Custom Control:
.head 3 -  ActiveX:
.head 2 -  Application Actions
.head 1 -  ! ------------------------------------------------------------------------------------------------------------------------------------------------
.head 1 -  ! Modifications History
.head 1 -  ! ====================================================================
.head 1 -  ! Date		Who	Ref	Brief Description
.head 1 +  ! ====================================================================
.head 2 -  !
.head 2 +  ! JABIRU
.head 3 -  ! 01.11.2002	DOD		Converted to CTD 2.1
.head 2 +  ! ADV332
.head 3 -  ! 10.01.2003	   SSP		   changed usernames and passwords to lower case
.head 2 +  ! ACA333
.head 3 -  ! 31.08.2004	   VSP	Build0031	   change default password of SIR to SIR (used to be SECRET)
.head 1 +  ! ACA34
.head 2 -  ! 28.04.2006	   VSP		change TIM MasterID and Password to timdb (used to be sysadm)
.head 2 -  ! 24.08.2006	   RBB		MS SQL Server db conversion 
.head 2 -  ! 30.08.2006	   RCJ		MSSQL server conversion - no changes made - no procedure for SetUpdate and SetDelete		
.head 1 +  ! ACA 4.3.003
.head 2 -  ! 25.05.2011	   RTF		SMTP connection userid and password added	
.head 1 -  ! One Scenario
.head 1 -  ! 14.03.2013	   RTF	SRI-ACROW-0395	Added EDS schema userid and password for acrow
.head 1 -  ! 08.02.2017	   VSP	12894 	Added AP SMTP and AR SMTP userid and password
.head 1 -  ! ------------------------------------------------------------------------------------------------------------------------------------------------
.head 1 -  ! test load
.head 1 -  ! ------------------------------------------------------------------------------------------------------------------------------------------------
